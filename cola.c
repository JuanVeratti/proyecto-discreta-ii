#include "AniquilamientoPositronicoIonizanteGravitatorio.h"
#include "cola.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Data structure to represent a queue
struct queue
{
    u32 *items;     // array to store queue elements
    u32 maxsize;    // maximum capacity of the queue
    u32 front;      // front points to the front element in the queue (if any)
    u32 rear;       // rear points to the last element in the queue
    u32 size;       // current capacity of the queue
};
 
// Utility function to initialize a queue
struct queue* newQueue(u32 size)
{
    struct queue *pt = (struct queue*)malloc(sizeof(struct queue));

    pt->items = (u32*)malloc(size * sizeof(u32));
    pt->maxsize = size;
    pt->front = 0;
    pt->rear = -1;
    pt->size = 0;
 
    return pt;
}
 
// Utility function to return the size of the queue
u32 size(struct queue *pt) {
    return pt->size;
}
 
// Utility function to check if the queue is empty or not
u32 isEmpty(struct queue *pt) {
    return !size(pt);
}
 
// Utility function to return the front element of the queue
u32 front(struct queue *pt)
{
    if (isEmpty(pt))
    {
        printf("Underflow\nProgram Terminated\n");
        return 0;
    }
 
    return pt->items[pt->front];
}
 
// Utility function to add an element `x` to the queue
void enqueue(struct queue *pt, u32 x)
{
    if (size(pt) == pt->maxsize)
    {
        printf("Overflow\nProgram Terminated\n");
        return;
    }
 
    // printf("Inserting %d\t", x);
 
    pt->rear = (pt->rear + 1) % pt->maxsize;    // circular queue
    pt->items[pt->rear] = x;
    pt->size++;
 
    // printf("front = %d, rear = %d\n", pt->front, pt->rear);
}
 
// Utility function to dequeue the front element
void dequeue(struct queue *pt)
{
    if (isEmpty(pt))    // front == rear
    {
        printf("Underflow\nProgram Terminated\n");
        return;
    }
 
    // printf("Removing %d\t", front(pt));
 
    pt->front = (pt->front + 1) % pt->maxsize;  // circular queue
    pt->size--;
 
    // printf("front = %d, rear = %d\n", pt->front, pt->rear);
}

void deleteQueue(queue pt){
  if (pt != NULL){
    if (pt->items != NULL){
      free(pt->items);
    }
    free(pt);
  }
}