#include "AniquilamientoPositronicoIonizanteGravitatorio.h"
#include "AlduinPaarthurnaxIrileth.h"
#include "cola.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static u32* BFS(Grafo G, u32 vertice, u32* coloreo){

  queue rqueue = newQueue(NumeroDeVertices(G));
  coloreo[vertice] = 1;
  enqueue(rqueue, vertice);
  while (!isEmpty(rqueue)){

    u32 k = front(rqueue);
    dequeue(rqueue);
    for (u32 j = 0; j < Grado(k, G); ++j){
      if (coloreo[IndiceONVecino(j, k, G)] == UINT32_MAX){
        coloreo[IndiceONVecino(j, k, G)] = 1 + coloreo[k];
        enqueue(rqueue, j);
      }else if (coloreo[IndiceONVecino(j, k, G)] == coloreo[k]){
        free(coloreo); coloreo = NULL;
        deleteQueue(rqueue); rqueue = NULL;
        return coloreo;
      }
    }

  }
  deleteQueue(rqueue); rqueue = NULL;

  return coloreo;
}

u32* Bipartito(Grafo G){

  u32* coloreo = malloc(NumeroDeVertices(G) * sizeof(u32));
  if (coloreo == NULL) {printf("Fallo al dar memoria en Bipartito\n"); return 0;}

  for (u32 i = 0; i < NumeroDeVertices(G); i++) coloreo[i] = UINT32_MAX;
  if (coloreo == NULL) {printf("Fallo al inicializar el coloreo del grafo.\n"); return 0;}

  for (u32 i = 0; i < NumeroDeVertices(G); ++i){
    if (coloreo[i] == UINT32_MAX) coloreo = BFS(G, i, coloreo);
    if (coloreo == NULL) break;
  }

  return coloreo;
}

// Retorna 1 si el coloreo es propio, 0 si no lo es
static u32 EsPropio(Grafo G, u32* coloreo){

  for (u32 k = 0; k < NumeroDeVertices(G); ++k){
    for (u32 j = 0; j < Grado(k, G); ++j){
      if (coloreo[k] == coloreo[IndiceONVecino(j, k, G)]){
        printf("****************coloreo[%u] = coloreo[%u]****************\n", k, IndiceONVecino(j, k, G));
        return 0;
      }
    }
  }
  return 1;
}

// Orden es el orden en que vamos a colorear, |Orden| = NumeroDeVertices(G). Es decir, es un orden de los vertices
// Coloreo ya tiene lugar asignado para poder escribir. No chequeamos esto
u32 Greedy(Grafo G, u32* Orden, u32* Coloreo){

  u32 colorMaximo, colorAUsar;

  u32* colorUsado = malloc((Delta(G) + 1) * sizeof(u32));
  if (colorUsado == NULL) {printf("Fallo al asignar memoria a arreglo auxiliar en Greedy.\n"); return UINT32_MAX;}
  for (u32 i = 0; i < (Delta(G) + 1); i++) colorUsado[i] = 0;

  for (u32 i = 0; i < NumeroDeVertices(G); i++) Coloreo[i] = UINT32_MAX;
  if (Coloreo == NULL) {printf("Fallo al inicializar el coloreo del grafo en Greedy.\n"); return UINT32_MAX;}

  // El primer vertice a procesar en Greedy es Orden[0]
  colorMaximo = 0;

  for (u32 k = 0; k < NumeroDeVertices(G); ++k){

    // Pongo en 1 a cada color que no este disponible
    for (u32 j = 0; j < Grado(Orden[k], G); ++j){
      if (Coloreo[IndiceONVecino(j, Orden[k], G)] != UINT32_MAX) colorUsado[Coloreo[IndiceONVecino(j, Orden[k], G)]] = 1;
    }

    // Itero hasta encontrar el primer color no usado y queda guardado en colorAUsar
    colorAUsar = 0;
    while (colorUsado[colorAUsar] != 0) ++colorAUsar;

    // Veo si es el colorMaximo (return de Greedy)
    if (colorAUsar > colorMaximo) colorMaximo = colorAUsar;
    // Coloreo el vertice K con el color primer color no usado encontrado
    Coloreo[Orden[k]] = colorAUsar;
    // Vuelvo a setear cada colorUsado a 0 para el prox. loop
    for (u32 i = 0; i < Grado(Orden[k], G); ++i){
      if (Coloreo[IndiceONVecino(i, Orden[k], G)] != UINT32_MAX) colorUsado[Coloreo[IndiceONVecino(i, Orden[k], G)]] = 0;
    }
  }

  free(colorUsado);
  colorUsado = NULL;
  u32 esProp = EsPropio(G, Coloreo);
  printf("\n");
  if (!esProp) {printf("No es propio\n"); return 0;}

  // Sumo porque estoy empezando a colorear desde el 0
  return colorMaximo + 1;
}

void AleatorizarKeys(u32 n, u32 R, u32* key){

  srand(R);

  for(u32 i = 0; i < n; i++){  //va desde cero hasta n-1
     key[i] = rand()%n; //valor random entre 0 y n-1 ?
  }
}

u32* PermutarColores(u32 n, u32* Coloreo, u32 R) {
  //u32* DiccionarioConversiones[n];
  u32* DiccionarioConversiones = malloc(n * sizeof(u32));
  u32* ColoreoNuevo = ColoreoNuevo = malloc(n * sizeof(u32));
  u32 valor;
  for (u32 i=0; i < n; i++) {
      //DiccionarioConversiones[i] = NULL;
      DiccionarioConversiones[i] = UINT32_MAX;
      //ColoreoNuevo[i] = NULL;
      ColoreoNuevo[i] = UINT32_MAX;
  }
  srand(R);
  for (u32 i=0; i < n; i++) {
      valor = Coloreo[i];
      if (valor >= n) {
        free(ColoreoNuevo); ColoreoNuevo = NULL;
        free(DiccionarioConversiones); DiccionarioConversiones = NULL;
        return NULL;
      }
      if (DiccionarioConversiones[valor] == UINT32_MAX) {
        //*DiccionarioConversiones[valor] = rand();
        DiccionarioConversiones[valor] = rand();
      }
    //ColoreoNuevo[i] = *DiccionarioConversiones[valor];
    ColoreoNuevo[i] = DiccionarioConversiones[valor];
  }

  free(DiccionarioConversiones);
  //return ColoreoNuevo[n];
  return ColoreoNuevo;
}

// lo pongo aca pero puede ir en otro archivo
static int Comparar_2(const void *a, const void *b){
    const ordenar ai = *(const ordenar *)a;
    const ordenar bi = *(const ordenar *)b;

    if (ai.v1 > bi.v1){
        return -1;
    }
    else if (ai.v1 < bi.v1){
        return 1;
    }
    else{
        return 0;
    }
}


char OrdenFromKey(u32 n,u32* key,u32* Orden){

 // IDEA: Hacer una estructura de datos que me guarde cada valor maximo con su indice y ordenarlo segun ese valor maximo
 ordenar *orden_aux;
  if(NULL == (orden_aux = malloc(n * sizeof(ordenar)))){
  return 1;
  }
 
 
    for (u32 i = 0; i < n; i++)
    {
        orden_aux[i].v2 = i;    // su indice original
        orden_aux[i].v1 = key[i]; // el valor que tenia antes
    }

    qsort(orden_aux, n, sizeof(ordenar) , Comparar_2);

    for (u32 i = 0; i < n; i++)
    {
        Orden[i] = orden_aux[i].v2;
    }
    

 /* checkeo invariante
    for (u32 i = 0; i < n-1; i++){
      assert(key[Orden[i]] >= key[Orden[i+1]]);
    }
*/
    free(orden_aux);
    orden_aux = NULL;

    return 0;
}

u32* RecoloreoCardinalidadDecrecienteBC(u32 n,u32* Coloreo){

   ordenar *color_counter = calloc(n , sizeof(ordenar));
   u32 color_index, used_counter, max_color = 0;
   u32 *new_color  = malloc(n * sizeof(u32));
   for (u32 i = 0; i < n; i++)
   {
       // voy a usar un array para contar la cantidad de apariciones de cierto color en un array
       // color_counter[Nombre_del_color] = k   , con k la cantidad de veces que aparece

       color_index = Coloreo[i];
       color_counter[color_index].v2 = Coloreo[i];
       color_counter[color_index].v1 += 1;

     // voy a almacenar el color maximo en una variable
     if(max_color < color_index){
         max_color = color_index;
     }

   }
    // ordeno de mayor a menor

    qsort(color_counter, max_color+1, sizeof(ordenar) , Comparar_2);  
    // map color => a quien me corresponde
    // used => 0 no esta asignado 1 si lo esta
    u32 map_color[max_color+1], used[max_color+1];

      for (u32 i = 0; i < max_color+1; i++)
    {
        used[i] = 0;
        map_color[i] = 0;
    }

    used_counter = 0;
    for( u32 i = 0 ; i < n; i++){

       // coloreo actual 
       color_index = Coloreo[i];
       if (color_index > n){
         free(color_counter); color_counter = NULL;
         return NULL;
       }
       if(used[color_index] == 0){
           map_color[color_index] = color_counter[used_counter].v2;
           used_counter++;
           used[color_index] = 1;
       }
       new_color[i] = map_color[color_index];

    }
 free(color_counter);
 color_counter = NULL;
 return new_color;
}