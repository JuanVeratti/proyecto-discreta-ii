#include "AniquilamientoPositronicoIonizanteGravitatorio.h"
#include "AlduinPaarthurnaxIrileth.h"
#include <time.h>
#include <stdio.h>

u32 ContadorDeGreedys = 0;

//Devuelve el mejor coloreo si todo bien, 0 si algo mal
u32 Recoloreo(Grafo G, u32* Orden, u32* Coloreo, u32 B, u32 P, u32 MC){
  u32 GreedyRes = Greedy(G, Orden, Coloreo); ContadorDeGreedys += 1;
  u32 GreedyResAnterior = GreedyRes;
	u32 *Key1, *Key2;
  printf("Greedy coloreo con %u colores (0)\n", GreedyRes);
  for (u32 k = 0; k < B; ++k){
    //1er recoloreo
    OrdenFromKey(NumeroDeVertices(G), Coloreo, Orden);
    GreedyRes = Greedy(G, Orden, Coloreo); ContadorDeGreedys += 1;
    printf("Greedy coloreo con %u colores (1)\n", GreedyRes);
    if (GreedyRes > GreedyResAnterior) return 0;
    //2do recoloreo
    GreedyResAnterior = GreedyRes;
    Key1 = PermutarColores(NumeroDeVertices(G), Coloreo, P);
    OrdenFromKey(NumeroDeVertices(G), Key1, Orden);
    GreedyRes = Greedy(G, Orden, Coloreo); ContadorDeGreedys += 1;
    printf("Greedy coloreo con %u colores (2)\n", GreedyRes);
    if (GreedyRes > GreedyResAnterior) return 0;
    //3er recoloreo
    GreedyResAnterior = GreedyRes;
    Key2 = RecoloreoCardinalidadDecrecienteBC(NumeroDeVertices(G), Coloreo);
    OrdenFromKey(NumeroDeVertices(G), Key2, Orden);
    GreedyRes = Greedy(G, Orden, Coloreo); ContadorDeGreedys += 1;
    printf("Greedy coloreo con %u colores (3)\n", GreedyRes);
    if (GreedyRes > GreedyResAnterior) return 0;
    free(Key1); free(Key2);
    Key1 = NULL; Key2 = NULL;
  }
  // free(Key1); Key1 = NULL;
  // free(Key2); Key2 = NULL;
  if (GreedyRes < MC) return GreedyRes;
  else return MC;
}

// void test_RecoloreoCardinalDecrecienteBC(u32 array[] ,u32 z){

//  printf("----------------------------------\n");
//  printf("TESTING: RECOLOREO CARDINAL DECRECIENTE\n");
//  u32* Coloreo_probar = malloc(z * sizeof(u32));

//  for (u32 i = 0; i < z; i++)
//  {
//     Coloreo_probar[i] = array[i];
//  }

//     printf("[");
//     for (u32 i = 0; i < z-1; i++){
//         printf(" %d, ",Coloreo_probar[i]);
//     }
//     printf(" %d ]\n", Coloreo_probar[z-1]);

//  RecoloreoCardinalidadDecrecienteBC(z, Coloreo_probar);


//     printf("[");
//     for (u32 i = 0; i < z-1; i++){
//         printf(" %d, ",Coloreo_probar[i]);
//     }
//     printf(" %d ] \n ", Coloreo_probar[z-1]);

//  free(Coloreo_probar);
//  Coloreo_probar = NULL;
//  printf("----------------------------------\n");
// }



// // usar para verificar con prints
// void test_orderfromkey_prints(u32 array[], u32 z){
// printf("----------------------------------\n");
// printf("TESTING: ORDER FROM KEY\n");
//  u32* key = malloc(z * sizeof(u32));
//  printf("key antes: [");
//  for (u32 i = 0; i < z; i++)
//  {
//     key[i] = array[i];
//     printf("%d, ", key[i]);
//  }
//  printf("]\n");
 
//  u32* orden = malloc(z * sizeof(u32));
//  OrdenFromKey(z,key, orden);
 
//  printf("orden despues: [");
//  for (u32 i = 0; i < z; i++)
//  {
//     printf("%d, ",orden[i]);
//  }
//  printf("]\n");

//  free(key);
//  free(orden);
//  orden= NULL;
//  key = NULL;
//  printf("----------------------------------\n");
// }

int main(int argc, char** argv){
  //argv[0] = nombreprog
  //argv[1] = a
  //argv[2] = b
  //argv[3] = p
  clock_t begin = clock();

  if (argc != 4) {printf("Asi no se carga el grafo!\n"); return 0;}
  
  Grafo grafito = ConstruccionDelGrafo();
  printf("Numero de vertices : %u\nNumero de lados : %u\nDelta del grafo %u\n",
          NumeroDeVertices(grafito), NumeroDeLados(grafito), Delta(grafito));

  u32 a = atoi(argv[1]), b = atoi(argv[2]), p = atoi(argv[3]);

  u32* Orden = malloc(NumeroDeVertices(grafito) * sizeof(u32));
  u32* Coloreo = malloc(NumeroDeVertices(grafito) * sizeof(u32));
  u32* ColoreoBip = malloc(NumeroDeVertices(grafito) * sizeof(u32));
  u32* Key = malloc(NumeroDeVertices(grafito) * sizeof(u32));
  u32 RecoloreoRes, MejorColoreo = NumeroDeVertices(grafito);

  ColoreoBip = Bipartito(grafito);
  if (ColoreoBip != NULL){
    printf("\nEs bipartito!\n");
    free(ColoreoBip);
    ColoreoBip = NULL;
    // if (NumeroDeVertices(grafito) < 101)
  }
  else {printf("\nNo es bipartito!\n"); free(ColoreoBip); ColoreoBip = NULL;}

  //orden natural
  for (u32 k = 0; k < NumeroDeVertices(grafito); ++k) Orden[k] = k;
  RecoloreoRes = Recoloreo(grafito, Orden, Coloreo, b, p, MejorColoreo);
  if (RecoloreoRes == 0){printf("Greedy no coloreo segun la hipotesis del VIT\n"); return 0;}
  if (RecoloreoRes < MejorColoreo) MejorColoreo = RecoloreoRes;

  //Welsh-Powell
  for (u32 k = 0; k < NumeroDeVertices(grafito); ++k) Key[k] = Grado(k, grafito);
  if (OrdenFromKey(NumeroDeVertices(grafito), Key, Orden) == 1) printf("Fallo en OrdenFromKey\n"); // 1 era fallo
  RecoloreoRes = Recoloreo(grafito, Orden, Coloreo, b, p, MejorColoreo);
  if (RecoloreoRes == 0){printf("Greedy no coloreo segun la hipotesis del VIT\n"); return 0;}
  if (RecoloreoRes < MejorColoreo) MejorColoreo = RecoloreoRes;

  //los otros a recoloreos
  for (u32 k = 0; k < a; ++k){
    RecoloreoRes = Recoloreo(grafito, Orden, Coloreo, b, p, MejorColoreo);
    if (RecoloreoRes == 0){printf("Greedy no coloreo segun la hipotesis del VIT\n"); return 0;}
    if (RecoloreoRes < MejorColoreo) MejorColoreo = RecoloreoRes;
  }

  printf("\nEl mejor recoloreo fue con %u colores\n", MejorColoreo);

  free(Key); Key = NULL;
  free(Orden); Orden = NULL;
  free(Coloreo); Coloreo = NULL;
  DestruccionDelGrafo(grafito);
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("\nTomo %fs hacer %u Greedys\n", time_spent, ContadorDeGreedys);


//  u32 array1[] = {99,12,54,2,7,1,8}; // ese array va a ser mi key
//  test_orderfromkey_prints(array1, 7);

//  u32 array2[] = {1 , 2, 0, 4, 3 , 2, 0 ,2, 0, 3, 2, 4, 3, 2 , 4, 3, 1 , 4 ,2 , 1 , 4 ,2, 3, 1, 4 , 4 ,2 ,2 ,2};
//  test_RecoloreoCardinalDecrecienteBC(array2,29);

  return 0;
}


	// Grafo grafito = ConstruccionDelGrafo();
  // // u32* bipartito_test;
  // u32* orden = malloc(NumeroDeVertices(grafito) * sizeof(u32));
  // u32* coloreo = malloc(NumeroDeVertices(grafito) * sizeof(u32));
  // u32 greedy_res = 0;
	// // printf("G's delta : %u\n", Delta(grafito));
	// // if (1) {printf("Number of edges of G : %u\n", NumeroDeLados(grafito));printf("Number of nodes of G : %u\n", NumeroDeVertices(grafito)); printf("Grado del primero vertice : %u\n", Grado(0, grafito));}
	// //rand() % 
	// // u32 j = 0;
	// // j = j;
	// // for (u32 i = 0; i < NumeroDeVertices(grafito); i++){
	// // 	for (u32 m = 0; m < Grado(i, grafito); m++){
	// // 		j = IndiceONVecino(m, i, grafito);
	// // 	}
	// // }
  // // for (u32 j = 0; j < NumeroDeVertices(grafito); j++) printf("Imprimiendo");
  // // bipartito_test = Bipartito(grafito);
  // // if (bipartito_test == NULL) printf(" :(\n");
  // // else {
  // //   for (u32 i = 0; i < NumeroDeVertices(grafito); i++) printf("coloreo[%u] = %u\n", Nombre(i, grafito), bipartito_test[i]);
  // //   printf("Es Bipartito! :)\n");
  // //   free(bipartito_test);
	// // }
  // u32* key = malloc(NumeroDeVertices(grafito) * sizeof(u32));
  // for (u32 k = 0; k < NumeroDeVertices(grafito); k++) key[k] = k;
  // // OrdenFromKey(NumeroDeVertices(grafito), key, orden);
  // // for (u32 k = 0; k < NumeroDeVertices(grafito); ++k) orden[NumeroDeVertices(grafito) - 1 - k] = k;
  // // for (u32 k = 0; k < NumeroDeVertices(grafito); ++k) printf("orden[%u] = %u\n", k, orden[k]);
  // //printf("Numero de vert %u\n", NumeroDeVertices(grafito));
  // greedy_res = Greedy(grafito, orden, coloreo);
  // printf("y Greedy coloreo con %u colores\n",greedy_res);
  // // for (u32 i = 0; i < NumeroDeVertices(grafito); i++){
  // //   printf("Grado del vertice %u = %u\n", Nombre(i, grafito), Grado(i, grafito));
  // //   if (Grado(i, grafito) > Delta(grafito)){printf("ACÁ!\n"); break;}
  // // }
  // // printf("Delta %u \n", Delta(grafito));

  // free(orden);
  // orden=NULL;
  // free(coloreo);
  // coloreo=NULL;
	// DestruccionDelGrafo(grafito);
