#include<stdlib.h>
#include<iostream>

typedef uint32_t u32; 

void AleatorizarKeys(u32 n, u32 R, u32* key){

    srand(R);

    for(u32 i = 0; i < n; i++){  //va desde cero hasta n-1
       key[i] = rand()%n; //valor random entre 0 y n-1 ?
    }
}

u32* PermutarColores(u32 n, u32* Coloreo, u32 R) { 
    u32* DiccionarioConversiones[n];
    u32* ColoreoNuevo[n];
    u32 valor; 
    for (u32 i=0; i < n; i++) { 
        DiccionarioConversiones[i] = NULL; 
        ColoreoNuevo[i] = NULL; 
    } 
    srand(R); 
    for (u32 i=0; i < n; i++) { 
        valor = Coloreo[i]; 
        if (DiccionarioConversiones[valor] == NULL) { 
            *DiccionarioConversiones[valor] = rand(); 
        } 
        ColoreoNuevo[i] = DiccionarioConversiones[valor]; 
    }
    return ColoreoNuevo[n]; 
} 
