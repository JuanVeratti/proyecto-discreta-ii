/**
*  @file helpers.h
*  @brief Funciones auxiliares
*/
#ifndef _HELPERS
#define _HELPERS

#include "AniquilamientoPositronicoIonizanteGravitatorio.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
* @brief Definición de edge
*/
typedef struct{
    u32 v1;
    u32 v2;
} edge;

/**
* @brief Compara el campo v1 de dos aristas distintas
* @return -1 si el primer vértice es menor al segundo
*           1 si el segundo vértice es menor al primero
*           0 si son iguales 
* @note En ConstruccionDelGrafo(), qsort ordena el arreglo
*       de vértices en base a la función Comparar  
*/
int Comparar(const void *a, const void *b);

/**
* @brief Busca un elemento dentro de un arreglo
* @param array Un arreglo de struct _NGV
* @param elem el elemento que se está buscando
* @param lft Índice izquierdo
* @param rgt Índice derecho
* @return Índice de elem en array
* @note Complejidad log(n). Se asume que el arreglo está ordenado.
*/
unsigned int BinarySearch(struct _NGV *array, unsigned int elem,
                                  unsigned int lft, unsigned int rgt);


/**
* @brief Si el retorno de scanf es negativo, printea "Error scanf"
* @param error Entero (retorno del scanf a chequear)
*/
void ErrorScan(int error);

/**
* @brief Saltea las líneas que empiezan con 'c' 
* @return Si algún scanf salió mal, se retorna -1
*/
int SaltearComentarios();

/**
* @brief Construye un arreglo de 2*m elementos de tipo edge,
*        leyendo desde stdin. No solo se guarda la arista
*        (a,b) sino a esta le sigue la arita (b,a). 
*        Esto es conveniente para llenar el arreglo de
*        vecinos en ConstruccionDelGrafo()
* @return Si el número de aristas es menor al número de líneas leídas desde stdin,
*         devuelve el arreglo. 
*         De lo contrario, devuelve NULL.
* @note Se pide memoria
*/
edge* ConstruccionDeArreglo(u32 m);

#endif