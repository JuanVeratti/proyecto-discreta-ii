/*
Implementacion tomada de https://www.techiedelight.com/circular-queue-implementation-c/
*/
#ifndef COLA_H_
#define COLA_H_

#include "AniquilamientoPositronicoIonizanteGravitatorio.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct queue *queue;

queue newQueue(u32 size);

u32 isEmpty(queue pt);

u32 front(queue pt);

void enqueue(queue pt, u32 x);

void dequeue(queue pt);

void deleteQueue(queue pt);

#endif