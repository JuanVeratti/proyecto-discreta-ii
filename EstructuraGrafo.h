#include <stdint.h>

typedef uint32_t u32;

struct _NGV {
    u32 nombre;
    u32 grado;
    u32* vecinos;
};

typedef struct GrafoSt {
    u32 v;
    u32 l;
    u32 d;
    struct _NGV* ngv; //arreglo de vertices
}GrafoSt;
