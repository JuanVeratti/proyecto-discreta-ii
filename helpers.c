#include "AniquilamientoPositronicoIonizanteGravitatorio.h"
#include "helpers.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int Comparar(const void *a, const void *b){
    const edge ai = *(const edge *)a;
    const edge bi = *(const edge *)b;

    if (ai.v1 < bi.v1){
        return -1;
    }
    else if (ai.v1 > bi.v1){
        return 1;
    }
    else{
        return 0;
    }
}

unsigned int BinarySearch(struct _NGV *array, unsigned int elem,
                                  unsigned int lft, unsigned int rgt){
    unsigned int index = 0;
    if (lft <= rgt){
        unsigned int mid = (lft + rgt) / 2;
        if (array[mid].nombre == elem) {
            index = mid;
        }
        else if (array[mid].nombre > elem){
            index = BinarySearch(array, elem, lft, mid - 1);
        }
        else if (array[mid].nombre < elem){
            index = BinarySearch(array, elem, mid + 1, rgt);
        }
    }
    return index;
}

void ErrorScan(int error){
    if (error < 0){
        printf("Error scanf\n");
    }
}

int SaltearComentarios(){
    int err0 = 0;
    int err1 = 0;
    int err2 = 0;
    int err_scan = 0;
    char *ind_linea = calloc(2, sizeof(char));

    err0 = scanf("%s", ind_linea);
    ErrorScan(err0);
    
    while (strcmp(ind_linea, "c") == 0){
        err1 = scanf("%*[^\n]");
        ErrorScan(err1);
        err2 = scanf("%s", ind_linea);
        ErrorScan(err2); 
    }

    if ((err0 < 0) || (err1 < 0) || (err2 < 0)){
        err_scan = -1;
    }

    free(ind_linea); 
    return err_scan;
}

edge* ConstruccionDeArreglo(u32 m){
    int err_scan = 0;
    unsigned int lineas = 0;
    unsigned int v = 0; 
    edge* edges;
    
    edges = calloc(2*m, sizeof(edge));

    //Guardo cada arista seguida de su arista inversa
    while (lineas < m && !feof(stdin)){
        err_scan = scanf("%*s %u %u", &edges[v].v1, &edges[v].v2);
        ErrorScan(err_scan);
        edges[v + 1].v1 = edges[v].v2;
        edges[v + 1].v2 = edges[v].v1;
        v = v + 2;
        lineas++;
    } 

    //si no habia cantidad necesaria de lados devolver NULL
    if (lineas < m){
        free(edges);
        return NULL;
    }

    return edges;
} 
