#include "AniquilamientoPositronicoIonizanteGravitatorio.h"
#include "helpers.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

Grafo ConstruccionDelGrafo(){   
    //Declaración de variables
    Grafo G_init = NULL;
    edge* edges;
    u32 delta = 1;
    u32 verticeActual = UINT32_MAX;
    unsigned int ngv = 0;
    unsigned int buscaVecinos = 0;
    int err_scan = 0;

    //inicialización del grafo
    G_init = calloc(1, sizeof(struct GrafoSt));

    /*
    * Lectura de archivo y carga de aristas en un 
    * arreglo de aristas
    */

    //Saltear lineas de comentarios
    err_scan = SaltearComentarios();
    //Chequeo de scanf
    if (err_scan < 0){
        free(G_init);
        return NULL;
    }

    //Carga de nro devértices y nro de lados
    err_scan = scanf("%*s %d %d\n", &G_init->v, &G_init->l);
    if (err_scan < 0){
        printf("Error scanf\n");
        return NULL;
    }

    /*Alloc un arreglo de 2*m para tener todos
    los vecinos (lema de apretón de manos)*/
    edges = ConstruccionDeArreglo(G_init->l);
    if (edges == NULL){
        free(G_init); 
        return NULL;
    }

    qsort(edges, 2 * G_init->l, sizeof(edge), Comparar);

    // Cargar arreglo ngv con los nombres y grados

    //Pido memoria para el arreglo ngv
    G_init->ngv = calloc(G_init->v, sizeof(struct _NGV));

    //Cargar ngv sin vecinos e ir calculando delta
    for (u32 i = 0; i < G_init->l * 2; i++){
        if (verticeActual != edges[i].v1){ //encuentro un nuevo vértice y actualizo verticeActual
            G_init->ngv[ngv].nombre = edges[i].v1;
            verticeActual = edges[i].v1;
            G_init->ngv[ngv].grado++;
            ngv++;
        }
        else{ //estoy en el mismo vértice
            G_init->ngv[ngv - 1].grado++;
            if (delta < G_init->ngv[ngv - 1].grado)
                delta = G_init->ngv[ngv - 1].grado;
        }
    }
    G_init->d = delta;

    /*
     * Carga del arreglo de vecinos con los 
     * índice de j-ésimo vecino en orden natural
     */

    for (u32 i = 0; i < G_init->v; i++){
        G_init->ngv[i].vecinos = calloc(G_init->ngv[i].grado, sizeof(u32));
        for (u32 j = 0; j < G_init->ngv[i].grado; j++){
            G_init->ngv[i].vecinos[j] = BinarySearch(G_init->ngv, 
                                        edges[buscaVecinos].v2, 0, G_init->v);
            buscaVecinos++;
        }
    }

    free(edges);
    edges = NULL;

    return G_init;
}

void DestruccionDelGrafo(Grafo G){
    for (u32 i = 0; i < G->v; i++){
        free(G->ngv[i].vecinos);
    }
    free(G->ngv);
    free(G);
}

u32 NumeroDeVertices(Grafo G){
    return G->v;
}

u32 NumeroDeLados(Grafo G){
    return G->l;
}

u32 Delta(Grafo G){
    return G->d;
}

u32 Nombre(u32 i, Grafo G){
    return G->ngv[i].nombre;
}

u32 Grado(u32 i, Grafo G){
    u32 grado = UINT32_MAX; 
    if (i < G->v){
        grado = G->ngv[i].grado;
    }
    return grado;
}

u32 IndiceONVecino(u32 j, u32 k, Grafo G){
    u32 index = UINT32_MAX;
    if (k < NumeroDeVertices(G) && j < Grado(k, G)){
        index = G->ngv[k].vecinos[j];
    }
    return index;
}
